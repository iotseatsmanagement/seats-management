
var application = function(items){
    var seats = (items.length > 0)? JSON.parse(items): [];
    var websiteURL = 'http://localhost:8000/api';

    var processData = (data) => {
        return JSON.parse(data);
    }

    function renderData(){
        jQuery('#seats').html('');
        for (let indexRow = 0; indexRow < seats.length; indexRow++) {
            const element = seats[indexRow];
            jQuery('#seats').append('<ul class="seats-row"></ul>');
            for (let indexColumn = 0; indexColumn < element.length; indexColumn++) {
                const elem = element[indexColumn];
                jQuery('#seats .seats-row:last-child').append('<li data-availability="'+elem+'"></li>');
            }
        }
    }

    function updateSeats(){
        return new Promise((resolve, reject) => {
            jQuery.ajax({
                method: "GET",
                url: websiteURL,
                success: function(result, status, xhr){
                    resolve( processData(result) );
                }
            });
        });
    }

    function sendUpdate() {
        return new Promise((resolve, reject) => {
            jQuery.ajax({
                method: "POST",
                url: websiteURL,
                data: {seats: JSON.stringify(seats)},
                success: function(result, status, xhr){
                    resolve( processData(result) );
                }
            });
        });
    }

    jQuery('#seats').on('click', 'li', function(){
        let column = jQuery(this).index();
        let row = jQuery(this).parent().index();
        
        seats[row][column] = ( seats[row][column] == 0 )? 1: 0;

        sendUpdate().then((successMessage) => {
            seats = successMessage;
            renderData();
        }).catch(e => {
            console.log(e);
        });
    });
}