import json
import os

import tornado.escape
import tornado.ioloop
import tornado.web

import serial
ser = serial.Serial('COM3',9600)

appTitle = "Seats Manager" # Application title
seatsGrid = 8 # Seats Grid size (led grid)

class Config():
    seats = []

class ApiHandler(tornado.web.RequestHandler):
    def initialize(self, data):
        self.data = data

    def get(self): # Writes the seats grid
        self.write(json.dumps(data.seats, separators=(',', ':')))

    def post(self): # Modify the localdata and update the grid
        data.seats = tornado.escape.json_decode(self.get_argument('seats'))

        output = ""
        for line in data.seats:
            line2 = ""
            for item in line:
                line2 += str(item)
            bin = int(line2, 2)
            output += str(bin)+","
        output += '.'
        ser.write(str.encode(output))

        self.write(json.dumps(data.seats, separators=(',', ':')))


class MainHandler(tornado.web.RequestHandler):
    def initialize(self, data):
        self.data = data

    def get(self):
        # Render web interface
        self.render('../web/index.html', title = appTitle, seats = data.seats);


class Application(tornado.web.Application):

    def __init__(self):
        # Define URLS
        handlers = [
            (r"/", MainHandler, dict(data=data)),
            (r"/api", ApiHandler, dict(data=data)),
        ]
        # Define URL for cached data CSS & JS
        settings = {
            "static_path": os.path.join(os.path.join(os.path.dirname(__file__), os.pardir), "web")
        }
        # Return application settings
        return tornado.web.Application.__init__(self, handlers, **settings)


if __name__ == "__main__":
    # Start storage
    data = Config()

    # Define seats
    data.seats = [[0 for x in range(seatsGrid)] for y in range(seatsGrid)]

    # Start server on port 8000
    app = tornado.httpserver.HTTPServer(Application())
    app.listen(8000)
    tornado.ioloop.IOLoop.current().start()