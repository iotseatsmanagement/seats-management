
#include "LedControl.h"

LedControl lc = LedControl(12, 11, 10, 2); // Pins: DIN,CLK,CS, # of Display connected

unsigned long delayTime = 500; // Delay between Frames

void setup()
{
  Serial.begin(9600);
  lc.shutdown(0, false); // Wake up displays
  lc.setIntensity(0, 5); // Set intensity levels
  lc.clearDisplay(0);  // Clear Displays
}

String matrix;

void loop()
{
  if (Serial.available()) {
    //index = Serial.parseInt();
    matrix = Serial.readStringUntil('.');
    int index = 0;
    int last = 0;
    int first = 0;
    for(int i = 0; i < 8; i++){
      last = matrix.indexOf(',', first);
      int nr = matrix.substring(first, last).toInt();
      lc.setRow(0, i, nr);
      first = last + 1;
    }

  }
}
