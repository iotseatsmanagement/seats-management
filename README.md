# Seats Management

## Descriere proiect
Aranjarea si evidenta locurilor libere intr-o sala de cinematograf. Se vor aprinde cu rosu led-urile care indica locurile deja ocupate. 
Locurile ocupate vor fi salvate intr-o baza de date MongoDB. Locurile se vor ocupa folosind o interfata web conectata prin Sockets, pe un server in Python folosind Tornado Web Server. O data ocupat un loc folosind interfata web, aceasta trimite un query de PUT catre server acesta salvand informatiile in baza de date si updateaza afisajul.

## De cumparat

- http://roboromania.ro/produs/modul-led-8x8-dot-matrix-display/
- http://roboromania.ro/produs/modul-led-8x8-dot-matrix-display-v2/
- http://roboromania.ro/produs/modul-led-8x32-dot-matrix-display/


## Cerinte Proiect

### Abstract 
My proposal is to create an internet of seats/spaces. Chairs, desks, and rooms would communicate their status indicating their availability; and people, most likely via their smart phones, would check those statuses, and in some cases perform other actions such as reserving those spaces. The particular example that inspired this proposal is the Cal student or group of students looking for a space to work in the various libraries on campus. Sometimes one may spend minutes just wandering, looking for a space.
During midterms and finals, this can be even more challenging and time-consuming. If students could just use their smart phone to see where available seats are, a good amount of time can be collectively saved.
It's interesting though how this idea can extend to various spaces, such as restaurants, cafes, buses, bathrooms, room reservations, etc. In terms of creating a prototype, the source (chair devices), transformer (server), and the consumer (smart phone app) are all reasonably doable parts, none that seem particularly hard or easy. In a real world launch, the sources and the server would have more challenges such as handling scale, power consumption, cost, additional sources (room devices, table devices, etc.).
### Sources 
The sources are objects that usually represent a space. This can be chairs, tables, and rooms. Currently, these objects are not expected to be smart and connected, so the actual sources would be connected devices that (discreetly) attach to these objects. A prototype could potentially be Arduino-based with a wifi module and sensors to detect occupancy (such as a weight sensor). The activity types produced by these sources would indicate occupancy status changes, such as taken or freed.
### Transformers
Currently, I imagine only one type of transformer, which is essentially a web server. The sources and the consumers both talk to the server. The server handles storing state, handling constraints, performing any data analysis. By mediating between source and consumer, the server allows the source to be simpler while still being able to represent activity types such as reserved, requested (a soft reservation), recently freed/taken. The server can also handle hierarchical constraints (room reserved overrides chairs free), time constraints (reservation schedule), and access constraints (students cannot make hard reservations, they must make a request).
### Customers
Consumers are most likely going to be smart phone apps that talk to the server. I can definitely imagine other interesting cases for consumers, such as a large wall-sized screen showing occupancy for the rooms in its building, but for now focus will be on the individual's smart phone app. I imagine the app to provide access to a large set of spaces, so that the student may search for his/her ideal spot. Initial prototypes will probably have all spaces public, but in a real world application, there would be limits to who can see what. Student consumers can make and cancel requests for spaces. Admin consumers can make reservations, handle requests, or close off spaces.